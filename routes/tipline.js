exports.index = function(req, res){
  res.render('tipline', { title: 'Taxpayers Union', subtitle:'' });
};

var qconf = require('qconf'),
    config = qconf();
var azure = require('azure');
var logtastic = require('../lib/db')('logtastic');



exports.saveTip = function (req, res) {
 
    if (req.body.honeypot.length > 0) {
        res.send(423);
        return;
    }

    try {
        var Guid = require('guid');

        var tips =  require('../lib/db')('tips');
        var tipster = {};
        logtastic.save({message:"at the start of post build"});
        tipster.name = req.body.name;
        tipster.phone_number = req.body.phone_number;
        tipster.email = req.body.email;
        tipster.message = req.body.message;
        tipster.verify = req.body.verify ? true : false;
        tipster.created_on = new Date();
        tipster.honeypot = req.body.honeypot;
        tipster.id = Guid.create().toString();
        tipster.ip_address = req.header('x-forwarded-for') || req.connection.remoteAddress;
        logtastic.save({message:"at the end of post build"});
        try {
            if (req.body.files) {
                tipster.savedFile = 'yes';
                logtastic.save(req.body.files);
                tipster.filelocation = req.files[0].path;
            }
        }
        catch (e) {
            logtastic.save({message:"failed files", error: e});
        }
        logtastic.save({message:"at the end of file check"});
        logtastic.save(tipster);
        tips.save(tipster, function (err, obj) {
            if(err) {
                res.send(200);
            } else {
                res.send(200);
            }
            var sender = require('../lib/email');
            logtastic.save({message:"at the end of tip save"});
            sender.sendEmail(tipster, function (err, obj) {

            });
        });
    } catch(failed) {
        logtastic.save({message:"in the catch", error: failed });
        res.redirect('http://taxpayers.org.nz/pages/tip-line');
    }
};